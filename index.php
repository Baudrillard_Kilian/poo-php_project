<?php
use MiladRahimi\PhpRouter\Router;
use App\Controllers\AnnonceController;
use App\Controllers\ConnectionController;
use App\Controllers\ReservationController;

// paramètres bdd
define( 'DB_HOST', 'localhost' );
define( 'DB_NAME', 'airbnb' );
define( 'DB_USER', 'root' );
define( 'DB_PASS', '' );

// paramètres PDO
define( 'PDO_ENGINE', 'mysql' );
define( 'PDO_OPTION', [
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
]);



// DS = Astuce : c'est un alias racourci pour plus de lisibilité
define('DS', DIRECTORY_SEPARATOR);
// Chemin vers la racine du projet
define('ROOT_PATH', dirname(__FILE__) . DS );

session_start();


spl_autoload_register();

require_once "vendor/autoload.php";


// Initialisation du routeur
$router = new Router();

$router
    ->get('/', AnnonceController::class . '@index')
    ->get('/annonces/{id}', AnnonceController::class . '@detail')
    ->get('/inscription', ConnectionController::class . '@inscription')
    ->post('/inscription', ConnectionController::class . '@inscriptionProcess')
    ->get('/connection', ConnectionController::class . '@connection')
    ->post('/connection', ConnectionController::class . '@connectionProcess')
    ->get('/deconnection', ConnectionController::class . '@deconnection')
    ->get('/reservation', ReservationController::class . '@reservation')
    ->get('/reservationForm/{id}', ReservationController::class . '@reservationForm')
    ->post('/reservationForm/{id}', ReservationController::class . '@reservationFormProcess');

$router->dispatch();

