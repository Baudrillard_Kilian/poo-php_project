-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           8.0.16 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour airbnb
CREATE DATABASE IF NOT EXISTS `airbnb` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `airbnb`;

-- Listage de la structure de la table airbnb. annonce
CREATE TABLE IF NOT EXISTS `annonce` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `adresse` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `prix` decimal(10,0) NOT NULL DEFAULT '0',
  `logementType` varchar(255) NOT NULL,
  `critere_id` int(11) unsigned NOT NULL,
  `owner_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `annonce.owner_id-users` (`owner_id`),
  KEY `annonce.critere_id-critere` (`critere_id`),
  CONSTRAINT `annonce.critere_id-critere` FOREIGN KEY (`critere_id`) REFERENCES `critere` (`id`),
  CONSTRAINT `annonce.owner_id-users` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table airbnb. critere
CREATE TABLE IF NOT EXISTS `critere` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `taille` int(11) unsigned NOT NULL,
  `couchage` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table airbnb. equipement
CREATE TABLE IF NOT EXISTS `equipement` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table airbnb. equipement_post
CREATE TABLE IF NOT EXISTS `equipement_post` (
  `equipement_id` int(11) unsigned NOT NULL,
  `annonce_id` int(11) unsigned NOT NULL,
  KEY `equipement_annonce.equipement_id-equipement` (`equipement_id`),
  KEY `equipement_annonce.annonce_id-anonce` (`annonce_id`),
  CONSTRAINT `equipement_annonce.annonce_id-anonce` FOREIGN KEY (`annonce_id`) REFERENCES `annonce` (`id`),
  CONSTRAINT `equipement_annonce.equipement_id-equipement` FOREIGN KEY (`equipement_id`) REFERENCES `equipement` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table airbnb. profils
CREATE TABLE IF NOT EXISTS `profils` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table airbnb. reservation
CREATE TABLE IF NOT EXISTS `reservation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dateDebut` datetime NOT NULL,
  `dateFin` datetime NOT NULL,
  `users_id` int(11) unsigned NOT NULL,
  `annonce_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `reservation.user_id-users` (`users_id`),
  KEY `reservation.annonce_id-annonce` (`annonce_id`),
  CONSTRAINT `reservation.annonce_id-annonce` FOREIGN KEY (`annonce_id`) REFERENCES `annonce` (`id`),
  CONSTRAINT `reservation.user_id-users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

-- Listage de la structure de la table airbnb. users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `role` int(11) unsigned NOT NULL,
  `profil_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `users.profil_id-profil` (`profil_id`),
  CONSTRAINT `users.profil_id-profil` FOREIGN KEY (`profil_id`) REFERENCES `profils` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Les données exportées n'étaient pas sélectionnées.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
