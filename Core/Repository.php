<?php

namespace Core;

use App\Models\Profil;
use App\Models\User;
use \PDO;

abstract class Repository
{
    protected PDO $db_cnx;

    public abstract function getTable(): string;

    public function __construct(PDO $pdo)
    {
        $this->db_cnx = $pdo;
    }

    public function create( IModel $object ): ?IModel
    {
        // Création de la liste des colonnes :
        $arr_class_vars = get_object_vars($object);
        $arr_class_vars_keys = array_keys($arr_class_vars);

        $str_columns = implode(',', $arr_class_vars_keys);

        // création des listes de variables et de valeurs
        $arr_vars = [];
        $arr_values = [];
        // on assemble les valeurs des propriétés dans un tableau indépendant des noms
        foreach ($arr_class_vars_keys as $property) {

            // construction de la requête à préparer
            array_push($arr_vars, ':' . $property);

            // préparation des données pour la requête
            // construction du tableau des valeurs pour PDO
            // [
            //    'nom_var1' => 'valeur_var1',
            //    'nom_var2' => 'valeur_var2',
            //    etc...
            // ]
            $arr_values[$property] = $object->$property;

        }

        $str_vars = implode(',', $arr_vars);

        // INSERT INTO ma_table (col_1, col_2) VALUES (val_col_1, val_col_2)
        // Version Requête préparée :
        // INSERT INTO ma_table (:col_1, :col_2) VALUES (:val_1, :val_2)
        // $this->$db_cnx->
        // sprintf() formate une chaîne selon un modèle avec des emplacements vides
        // @link https://php.net/manual/en/function.sprintf.php
        $query = sprintf(
            'INSERT INTO %s (%s) VALUES (%s)',
            $this->getTable(),   // premier $s
            $str_columns,        // deuxième $s
            $str_vars          // troisière $s
        );


        $sth = $this->db_cnx->prepare($query);
        // exécution de la requête avec remplacement des variables
        // PDO va ajouter automatiquement les '' aux valeurs
        $sth->execute($arr_values);

        if ($sth && $sth->errorCode() === PDO::ERR_NONE) {
            $object->id = $this->db_cnx->lastInsertId();
            return $object;
        }

        return null;


    }

    /**
     * Recupère une liste complete "d'entité" f(u, type depuis la BDD
     * @param string $classname (Nom complet de la classe avec le chemin de namespace)
     * @return array Tableau d'objet du type passé en argument
     */
    protected function readAll(string $classname): array
    {
        // Tableau de résultats
        $arr_objects = [];

        $query = 'SELECT * FROM ' . $this->getTable();

        // Exécution de la requête
        $sth = $this->db_cnx->query($query);

        // a utilisé si il y a erreur pour la voir
        // var_dump($this->db_cnx->errorInfo());

        // Si il y a une erreur, on renvoie un tableau vide
        if (!$sth || $sth->errorCode() !== PDO::ERR_NONE) {
            return $arr_objects;
        }

        // Si la requête a fonctionné, on traite le résultat
        while ($row = $sth->fetch()) {
            // ex: $classname contient 'User', alors PHP va exécuter "new User()"
            $obj_row = new $classname($row);
            $arr_objects[] = $obj_row;
        }

        return $arr_objects;
    }


    protected function readById(int $id, string $classname): ?IModel
    {


        $query = sprintf('SELECT * FROM %s WHERE id=:id',
            $this->getTable()
        );

        //en cas d'erreur de preparation retourne null
        $sth = $this->db_cnx->prepare($query);
        if (!$sth) {
            return null;
        }

        $sth->bindValue('id', $id, PDO::PARAM_INT);

        // execution de la requête préparé
        $sth->execute();

        //En cas d'erreur d'execution retourne null
        if ($sth->errorCode() !== PDO::ERR_NONE) {
            return null;
        }

        $row = $sth->fetch();
        if ( !$row ) {
            return null;
        }

        /*
        // Pour débugguer
        $object = new $classname( $row );
        var_dump($object)
        return $object;
        */

        return new $classname($row);


    }

    public function verifEmail(string $email): ?User
    {
        $query = sprintf('SELECT * FROM %s WHERE email=:email',
        $this->getTable()
        );

        $sth = $this->db_cnx->prepare( $query );
        if( !$sth ) {
            return null;
        }

        // Attachement d'un paramètre avec précision de type
        $sth->bindValue( 'email', $email, PDO::PARAM_STR );

        // Exécution de la requête préparée
        $sth->execute();

        // En cas d'erreur du serveur SQL on retourne null
        if( $sth->errorCode() !== PDO::ERR_NONE ) {
            return null;
        }

        $row = $sth->fetch();
        if( !$row ) {
            return null;
        }

        return new User( $row );

    }

    public function verifUsername(string $username): ?User
    {
        $query = sprintf('SELECT * FROM %s WHERE username=:username',
        $this->getTable()
        );

        $sth = $this->db_cnx->prepare( $query );
        if( !$sth ) {
            return null;
        }

        // Attachement d'un paramètre avec précision de type
        $sth->bindValue( 'username', $username, PDO::PARAM_STR );

        // Exécution de la requête préparée
        $sth->execute();

        // En cas d'erreur du serveur SQL on retourne null
        if( $sth->errorCode() !== PDO::ERR_NONE ) {
            return null;
        }

        $row = $sth->fetch();
        if( !$row ) {
            return null;
        }

        return new User( $row );

    }

    public function login(string $username, string $password): ?User
    {
        $query = sprintf(
            'SELECT * FROM %s WHERE username=:username AND password=:password',
            $this->getTable()
        );

        $sth = $this->db_cnx->prepare( $query );
        if( !$sth ) {
            return null;
        }

        // Attachement d'un paramètre avec précision de type
        $sth->bindValue( 'username', $username, PDO::PARAM_STR );
        $sth->bindValue( 'password', hash('sha512',$password), PDO::PARAM_STR );

        // Exécution de la requête préparée
        $sth->execute();

        // En cas d'erreur du serveur SQL on retourne null
        if( $sth->errorCode() !== PDO::ERR_NONE ) {
            return null;
        }

        $row = $sth->fetch();
        if( !$row ) {
            return null;
        }

        return new User( $row );

    }



    public function update( IModel $object): ?IModel
    {
        // Création de la liste des colonnes :
        $arr_class_vars = get_object_vars($object);
        $arr_class_vars_keys = array_keys($arr_class_vars);

        // Création des listes des couples colonne/variable
        // [ "username=:username", ... ]
        $arr_vars = [];
        // Tableau pour le PDO
        $arr_values = [];
        // on assemble les valeurs des propriétés dans un tableau indépendant des noms
        foreach ($arr_class_vars_keys as $property) {
            $arr_values[ $property ] = $object->$property;

            if ($property === 'id') {
                continue;
            }

            $value = sprintf('%1$s=:%1$s', $property );
            $arr_vars[] = $value;

        }

        $str_vars = implode(',', $arr_vars);

        $query = sprintf(
            'UPDATE %s SET %s WHERE id=:id' ,
            $this->getTable(),
            $str_vars
        );

        // Préparation de la requête
        $sth = $this->db_cnx->prepare($query);
        // exécution de la requête avec remplacement des variables
        // PDO va ajouter automatiquement les '' aux valeurs
        $sth->execute($arr_values);

        if ($sth && $sth->errorCode() === PDO::ERR_NONE) {
            return $object;
        }

        return null;

    }

    public function delete(int $id): bool
    {
        $query = sprintf('DELETE FROM %s WHERE id=:id', $this->getTable() );
        $sth = $this->db_cnx->prepare( $query );

        // Si la préparation échoue, on renvoie false
        if( !$sth ) {
            return false;
        }

        $sth->bindValue( 'id', $id, PDO::PARAM_INT );

        $sth->execute();

        return $sth->errorCode() === PDO::ERR_NONE;

    }




}
