<?php


namespace Core;

use App\Repository\RepositoryManager;


abstract class Controller
{
    protected RepositoryManager $rm;

    public function __construct()
    {
        $this->rm = RepositoryManager::getRm();
    }
}