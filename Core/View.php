<?php


namespace Core;


class View
{
    private const VIEW_PATH = ROOT_PATH . 'App' . DS . 'Views' . DS;

    private string $_path_file;

    private bool $_view_file_exists = false;
    public function getViewFileExists(): bool { return $this->_view_file_exists; }

    public function __construct( string $view_name)
    {
        $this->_path_file = self::VIEW_PATH . $view_name . '.php';

        // Test de l'existance du fichier
        $this->_view_file_exists = is_readable($this->_path_file);
    }

    public function render( array $view_data = [] ):void
    {
        if (!$this->_view_file_exists) {
            echo 'une erreur s\'est produite';
            return;
        }
        //Crée des variables a partir d'un tableau assiociatif
        // Extract crée les variables $jesaispas $
        extract($view_data);

        require_once $this->_path_file;
    }
}