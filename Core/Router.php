<?php

namespace Core;

class Router
{
    private static ?self $instance = null;

    // Liste des routes géré par l'application
    private array $_routes = [];

    public string $controllers_namespace = '\\';

    public static function get(): self
    {
        if ( is_null(self::$instance) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Declare une route
     *
     * @param string $uri URI de la route
     * @param string $controller Nom du controller a
     * @param string $action Nom de l'action (méthode
     */
    public function registerRoute( string $uri, string $controller, string $action )
    {
        $route = new Route($uri, $controller, $action);
        $this->_routes[] = $route;
    }

    /**
     * Démarage du routeur
     */
    public function start(): void
    {
        // URL demandée
        $request_uri = $_SERVER[ 'REQUEST_URI' ];

        // Flag URL trouvée
        $url_found = false;

        // Parcours des routes à la recherche de l'URL demandée
        foreach( $this->_routes as $route ) {
            // Tant que l'URL demandé ne correspond pas, on passe au tour suivant
            if( $route->uri !== $request_uri ) {
                continue;
            }

            $controller_name = $this->controllers_namespace . '\\' . $route->controller;
            $action = $route->action;

            $controller = new $controller_name();
            $controller->$action();
            break;


        }


    }



    private function __construct() { }

    private function __clone() { }

    private function __wakeup() { }
}