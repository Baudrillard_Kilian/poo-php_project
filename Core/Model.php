<?php


namespace Core;


abstract class Model implements IModel
{
    public ?int $id;

    public function __construct( array $data = [] )
    {
        $this->hydrate( $data );
    }

    /**
     * @param array $data
     */
    private function hydrate( array $data ): void
    {
        foreach ( $data as $column => $value) {
            if ( property_exists(get_called_class(), $column ) ) {
                $this->$column = $value;
            }
        }
    }


}