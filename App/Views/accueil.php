<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="css/">
    <title> <?php echo $html_title ?> </title>
</head>
<body>
    <header>
        <nav>
            <?php require_once 'App/Views/menu.php'; ?>

        </nav>
    </header>
    <main>
        <h1><?php echo $html_h1 ?></h1>
<?php if ( count($annonce) > 0 ) : ?>
        <ul>
            <?php foreach ($annonce as $a) : ?>
                <li>
                    adresse :
                    <?php echo $a->adresse; ?>
                    <br>
                    Description :
                    <?php echo $a->description; ?>
                    <br>
                    Prix :
                    <?php echo $a->prix; ?>
                    <br>
                    Type de logement
                    <?php echo $a->logementType; ?>
                    <br>
                    <a href="/annonces/<?php echo $a->id ?>"> détails</a>
                </li>
                <br>
            <?php endforeach; ?>
        </ul>
<?php endif; ?>
    </main>
</body>
</html>