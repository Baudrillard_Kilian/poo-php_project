<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Connection</title>
</head>
<body>
    <header>
        <a href="/">Retour</a>
        <h1>Connection</h1>
    </header>
    <main>
        <form method="post">
            <label for="username" >
                <input type="text" name="username">
            </label>
            <label for="password">
                <input type="password" name="password">
            </label>
            <button type="submit">Connection</button>
        </form>
    </main>
</body>
</html>
