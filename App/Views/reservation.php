<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Document</title>
</head>
<body>
    <a href="/">Retour</a>
    <?php if ($_SESSION['role'] == 2) : ?>
    <h1>Vos Reservation</h1>
    <?php else: ?>
    <h1>Les Rendez-vous </h1>
    <?php endif; ?>
    <?php if( count( $reservation ) > 0) : ?>
        <ul>
        <?php foreach ($reservation as $r) : ?>
            <li>
                <?php echo $r->getUser()->getProfil()->firstname . ' ' . $r->getUser()->getProfil()->lastname?>
            </li>
            <li>
                <?php echo $r->getUser()->getProfil()->phone?>
            </li>
            <li>
                de : <?php echo $r->dateDebut?> au : <?php echo $r->dateFin?>
            </li>
            <li>
                <?php echo $r->getAnnonce()->adresse ?>
            </li>
            <br>
            <br>
        <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</body>
</html>
