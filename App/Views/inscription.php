<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Document</title>
</head>
<body>
    <main>
        <a href="/">Retour</a>
        <h1> Inscription </h1>
        <h2>Page D'inscription</h2>
        <form method="post" novalidate>
            <label for="lastname">
                Nom : <br>
                <input type="text" name="lastname">
            </label>
            <br>
            <label for="firstname">
                Prénom : <br>
                <input type="text" name="firstname">
            </label>
            <br>
            <label for="phone">
                Phone : <br>
                <input type="text" name="phone">
            </label>
            <br>
            <h3>Identifitation </h3>
            <label for="username">
                Username : <br>
                <input type="text" name="username">
            </label>
            <br>
            <label for="password">
                Password : <br>
                <input type="password" name="password">
            </label>
            <br>
            <label for="email">
                Email : <br>
                <input type="email" name="email">
            </label>
            <br>
                Votre type de compte
            <label for="role">
                <select id="role" name="role">
                    <option value="2" >Utilisateur</option>
                    <option value="1" >Annonceur</option>
                </select>
            </label>
            <br>
            <button type="submit">S'inscrire</button>
        </form>
    </main>
</body>
</html>
