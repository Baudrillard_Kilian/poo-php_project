<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title> <?php echo $html_title ?> </title>
</head>
<body>
<main>
    <a href="/">Retour</a>
    <h1><?php echo $html_h1 ?></h1>
    <div>
    <!-- L'annonce -->
    <?php if($annonce) : ?>
        Adresse :
        <?php echo $annonce->adresse ?>
        <br>
        Description :
        <?php echo $annonce->description ?>
        <br>
        Prix :
        <?php echo $annonce->prix ?>
        <br>
        Type de logement :
        <?php echo $annonce->logementType ?>
        <br>
        <!-- les critere -->
        Taille du logement
        <?php echo $annonce->getCritere()->taille?>
        <br>
        Nombre de Chambre
        <?php echo $annonce->getCritere()->couchage?>
    </div>
    <div>
        <h2> Information sur le vendeur</h2>
        <?php echo $annonce->getUser()->getProfil()->firstname . ' ' . $annonce->getUser()->getProfil()->lastname ?>
        <br>
        <?php echo $annonce->getUser()->getProfil()->phone?>
        <br>
        <?php echo $annonce->getUser()->email ?>
        <br>
    </div>
        <?php if(isset( $_SESSION['login'])) :?>
            <?php $_SESSION['annonce_id'] = $annonce->id ?>
            <?php if( $_SESSION['role'] == 2) : ?>
            <div>
                <a href="/reservationForm/<?php echo $annonce->id ?>">Reservez une chambre</a>
            </div>
            <?php endif; ?>
        <?php endif; ?>


    <?php endif; ?>


</main>

</body>
</html>
