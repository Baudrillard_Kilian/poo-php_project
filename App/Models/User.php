<?php


namespace App\Models;


use App\Repository\RepositoryManager;
use Core\IModel;
use Core\Model;

class User extends Model implements IModel
{
    public string $username;
    public string $password;
    public string $email;
    public string $role;
    public ?string $profil_id;

    private ?Profil $profils = null;

    public function setProfil(?Profil $profils): self
    {
        $this->profils = $profils;
        return $this;
    }

    public function getProfil(): ?Profil
    {
        if(is_null($this->profils)){
            $this->profils = RepositoryManager::getRm()->getProfilRepo()->findById($this->profil_id);
        }
        return $this->profils;
    }

}