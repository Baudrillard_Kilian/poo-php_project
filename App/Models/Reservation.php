<?php


namespace App\Models;


use App\Repository\RepositoryManager;
use Core\IModel;
use Core\Model;

class Reservation extends Model implements IModel
{
    public string $dateDebut;
    public string $dateFin;
    public ?string $users_id;
    public ?string $annonce_id;

    private ?User $users = null;

    private ?Annonce $annonce = null;


    public function setUser(?User $users): self
    {
        $this->users = $users;
        return $this;
    }

    public function getUser(): ?User
    {
        if (is_null($this->users) ){
            $this->users = RepositoryManager::getRm()->getUserRepo()->findById($this->users_id);
        }
        return $this->users;
    }

    public function setAnnonce(?Annonce $annonce): self
    {
        $this->annonce = $annonce;
        return $this;
    }

    public function getAnnonce(): ?Annonce
    {
        if (is_null($this->annonce) ){
            $this->annonce = RepositoryManager::getRm()->getAnnonceRepo()->findById($this->annonce_id);
        }
        return $this->annonce;
    }

}