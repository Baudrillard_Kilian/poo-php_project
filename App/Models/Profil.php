<?php


namespace App\Models;


use Core\IModel;
use Core\Model;

class Profil extends Model implements IModel
{
    public string $firstname;
    public string $lastname;
    public string $phone;
}