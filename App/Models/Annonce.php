<?php


namespace App\Models;

use App\Repository\RepositoryManager;
use Core\IModel;
use Core\Model;

class Annonce extends Model implements IModel
{
    public string $adresse;
    public string $description;
    public string $prix;
    public string $logementType;
    public string $critere_id;
    public string $owner_id;

    private ?Critere $critere = null;

    private ?User $users = null;

    public function setCritere(?Critere $critere): self
    {
        $this->critere = $critere;
        return $this;
    }

    public function getCritere(): ?Critere
    {
        if (is_null($this->critere) ){
            $this->critere = RepositoryManager::getRm()->getCritereRepo()->findById($this->critere_id);
        }
        return $this->critere;
    }


    public function setUser(?User $users): self
    {
        $this->users = $users;
        return $this;
    }

    public function getUser(): ?User
    {
        if (is_null($this->users) ){
            $this->users = RepositoryManager::getRm()->getUserRepo()->findById($this->owner_id);
        }
        return $this->users;
    }
}

