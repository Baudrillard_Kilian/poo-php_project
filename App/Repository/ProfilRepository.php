<?php


namespace App\Repository;


use App\Models\Profil;
use Core\Repository;

class ProfilRepository extends Repository
{

    public function getTable(): string
    {
        return 'profils';
    }

    public function findById(int $id): ?Profil
    {
        return $this->readById($id, Profil::class);
    }
}