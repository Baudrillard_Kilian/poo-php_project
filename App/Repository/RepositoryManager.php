<?php


namespace App\Repository;


use Core\Database;

class RepositoryManager
{
    private static ?self $instance = null;

    private AnnonceRepository $annonce_repo;
    public function getAnnonceRepo(): AnnonceRepository
    {
        return $this->annonce_repo;
    }

    private CritereRepository $critere_repo;
    public function getCritereRepo(): CritereRepository
    {
        return $this->critere_repo;
    }

    private UserRepository $user_repo;
    public function getUserRepo(): UserRepository
    {
        return $this->user_repo;
    }

    private ProfilRepository $profil_repo;
    public function getProfilRepo(): ProfilRepository
    {
        return $this->profil_repo;
    }



    private ReservationRepository $reservation_repo;
    public function getReservationRepo(): ReservationRepository
    {
        return $this->reservation_repo;
    }



    public static function getRm(): self
    {
        if (is_null(self::$instance) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct()
    {
        $pdo = Database::get();

        $this->annonce_repo = new AnnonceRepository($pdo);
        $this->critere_repo = new CritereRepository($pdo);
        $this->user_repo = new UserRepository($pdo);
        $this->profil_repo = new ProfilRepository($pdo);
        $this->reservation_repo = new ReservationRepository($pdo);

    }

    private function __clone() { }
    private function __wakeup() { }
}