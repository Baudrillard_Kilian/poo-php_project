<?php


namespace App\Repository;


use App\Models\Annonce;
use Core\Repository;

class AnnonceRepository extends Repository
{

    public function getTable(): string
    {
        return "annonce";
    }

    public function findAll(): array
    {
        return $this->readAll( Annonce::class );
    }

    public function findById( int $id ): ?Annonce
    {
        return $this->readById( $id, Annonce::class );
    }

}