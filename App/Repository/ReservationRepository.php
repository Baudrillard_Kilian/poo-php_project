<?php


namespace App\Repository;


use App\Models\Annonce;
use App\Models\Reservation;
use App\Models\User;
use Core\Repository;
use Core\View;

class ReservationRepository extends Repository
{
    public function getTable(): string
    {
        return 'reservation';
    }

    public function findAll() : array
    {
        return $this->readAll(Reservation::class);
    }

    //public function findByUser(int $id): ?User
    //{
    //    return $this->readByUser($id, User::class);
    //}

}