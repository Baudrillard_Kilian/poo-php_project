<?php


namespace App\Repository;


use App\Models\User;
use Core\Repository;

class UserRepository extends Repository
{

    public function getTable(): string
    {
        return "users";
    }

    public function findById( int $id ): ?User
    {
        return $this->readById($id, User::class);
    }

}