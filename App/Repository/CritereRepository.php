<?php


namespace App\Repository;


use App\Models\Critere;
use Core\Repository;

class CritereRepository extends Repository
{
    public function getTable(): string
    {
        return "critere";
    }


    public function findById( int $id ): ?Critere
    {
        return $this->readById( $id, Critere::class );
    }

}