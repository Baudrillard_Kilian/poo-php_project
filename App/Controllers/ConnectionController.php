<?php


namespace App\Controllers;


use App\Models\Profil;
use App\Models\User;
use Core\Controller;
use Core\View;
use Zend\Diactoros\ServerRequest;

class ConnectionController extends Controller
{
    public function connection(): void
    {
        $view = new View('connection');

        $view->render();

    }
    public function connectionProcess(ServerRequest $request): void
    {
        $data = $request->getParsedBody();


        if (isset($data['username']) && isset($data['password'])){
            $username = $data["username"];
            $password = $data['password'];
            if (!empty($username) && !empty($password)){
                $user = $this->rm->getUserRepo()->login($username, $password);
                if (isset($user)){
                    $_SESSION['login'] = $user;
                    $_SESSION['role'] = $user->role;
                    $_SESSION['id'] = $user->id;
                    header('Location:/');
                }
            }
        }
    }

    public function deconnection(): void
    {
        $view = new View('deconnection');


        $view->render();


    }

        public function inscription(): void
    {
        $view = new View('inscription');

        $view->render();
    }

    public function inscriptionProcess(ServerRequest $request): void
    {

        $data = $request->getParsedBody();

        if (isset($data["firstname"]) && isset($data['lastname']) && isset($data['phone'])) {
            $firstname = $data["firstname"];
            $lastname = $data["lastname"];
            $phone = $data["phone"];
            if (!empty($firstname) && !empty($lastname) && !empty($phone)) {
                $profil = [
                    'id' => null,
                    'firstname' => $firstname,
                    'lastname' => $lastname,
                    'phone' => $phone
                ];

                if(isset($data['username']) && isset($data['password']) && isset($data['email']) && isset($data['role']) ) {
                    $username = $data['username'];
                    $password = $data['password'];
                    $email = $data['email'];
                    $role = $data['role'];
                    $profildata = new Profil($profil);

                    if (!empty($username) && !empty(hash('sha512', $password) ) && !empty($email) && !empty($role)) {

                        $user = [
                            'id' => null,
                            'username' => $username,
                            'password' => hash('sha512', $password),
                            'email' => $email,
                            'role' => $role,
                            'profil_id' => null
                        ];
                        $userdata = new User($user);

                        $verifEmail = $this->rm->getUserRepo()->verifEmail($user['email']);

                        if (isset($user['email']) != $verifEmail) {
                            $verifUsername = $this->rm->getUserRepo()->verifUsername($user['username']);
                            if (isset($user['username']) != $verifUsername) {
                                $new_profil = $this->rm->getProfilRepo()->create($profildata);
                                $userdata->profil_id = $new_profil->id;
                                $new_user = $this->rm->getUserRepo()->create($userdata);

                                header('Location:/');
                            }
                        }

                    }

                }
            }


        }
    }

}