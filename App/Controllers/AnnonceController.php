<?php


namespace App\Controllers;

use Core\Controller;
use Core\View;


class AnnonceController extends Controller
{
    public function index(): void
    {
        $view = new View('accueil');

        $view_data = [
            'html_title' => 'Liste des Annonce',
            'html_h1' => 'Les Annonces Disponible',
            'annonce' => $this->rm->getAnnonceRepo()->findAll()
        ];

        $view->render( $view_data );
    }

    public function detail(int $id): void
    {
        $view = new View('annonceDetail');

        $view_data = [
            'html_title' => 'Détail de L\'annonce',
            'html_h1' => 'Annonce Complète',
            'annonce' => $this->rm->getAnnonceRepo()->findById($id),

        ];

        $view->render( $view_data );
    }

}