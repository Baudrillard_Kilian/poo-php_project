<?php


namespace App\Controllers;


use App\Models\Reservation;
use App\Models\User;
use Core\Controller;
use Core\View;
use Zend\Diactoros\ServerRequest;

class ReservationController extends Controller
{
    public function reservation():void
    {
        $view = new View('reservation');

        $view_data = [
            'reservation' =>$this->rm->getReservationRepo()->findAll()
        ];

        $view->render( $view_data );
    }

    public function reservationForm(): void
    {
        $view = new View('reservationForm');

        $view->render();
    }

    public function reservationFormProcess(ServerRequest $request): void
    {

        $data = $request->getParsedBody();

        $user = $_SESSION['id'];

        $annonce = $_SESSION['annonce_id'];

        if (isset($data['dateDebut']) && isset($data['dateFin'])) {
            $dateDebut = $data['dateDebut'];
            $dateFin = $data['dateFin'];

            var_dump($data);
            if (!empty($dateDebut) && !empty($dateFin)) {
                $reservation = [
                    'id' => null,
                    'dateDebut' => $dateDebut,
                    'dateFin' => $dateFin,
                    'users_id' => null,
                    'annonce_id' => null
                ];
                $reservdata = new Reservation($reservation);
                var_dump( $reservation);
                if ($dateDebut <= $dateFin) {
                    echo "datedebut < ou = a datefin";
                    $reservdata->users_id = $user;
                    $reservdata->annonce_id = $annonce;

                    var_dump($reservdata);
                    $new_reservation = $this->rm->getReservationRepo()->create($reservdata);

                    header('Location: /');
                }
            }
        }

    }
}